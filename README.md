 These are the data files used to populate the [Oilseed Rape Developmental Expression Resource (ORDER)](http://order.jic.ac.uk).

 *Brassica napus* gene expression data, gene details and orthology information, and website page content are included in the repository. The format and directory structure of the files are suitable for use with [AionPlot](https://github.com/marc-jones/aionplot).

 To create a subset of this data that focuses on flowering time genes, use the scripts available in [this GitHub repository](https://github.com/marc-jones/order-data-file-creation).
