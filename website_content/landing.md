<h1 id="sec0" align='center'>ORDER</h1>

<h3 id="subhead" align="center">Oilseed Rape Developmental Expression Resource</h3>

<br>

<img class="img-responsive center-block" style="width:400px;max-width:100%" src="{{ url_for('website_content', filename='bnapus_flower.svg') }}" alt="Brassica napus">

<br>

The Oilseed Rape Developmental Expression Resource (ORDER) was created to provide access to the developmental transcriptome of *Brassica napus* during the floral transition.

### Guide

The About tab presents information about how the experiment was carried out, such as which *Brassica napus* varieties were grown, how the samples were collected and how gene expression levels were quantified.

The How To Use tab explains how to use the website to search the database.
This is the place to go if you have questions about the user interface of the website.

The final three tabs (Search, BLAST Search and Table) are the tabs you'll be using to search the database.

### Contact

Please could you direct all enquiries, bug reports and interesting findings which relate to this dataset to [Marc Jones](mailto:m4rcj0n35@gmail.com).

### AionPlot

ORDER is an instance of the time series visualization and dissemination platform AionPlot.
