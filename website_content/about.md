<center><h2>About</h2></center>

This is a web app to facilitate interrogation of time series data collected from *Brassica napus*.
The data was collected from the cultivars **Tapidor** (a winter cultivar) and **Westar** (a spring cultivar) with the aim of deciphering how the flowering time gene regulatory network is constructed, and how it differs from the network elucidated in *Arabidopsis thaliana*.
An overview of the plant growth regime and data processing pipeline is provided below, but for further details please consult our published work on the project (<a href="https://doi.org/10.1111/tpj.14020", target="_blank">Jones et al. 2018</a>).

### Growth Details

Plants were germinated and grown in a glasshouse for two weeks before being transferred to a controlled environment room to undergo vernalization.
Plants from both the winter and spring varieties were vernalized to allow more accurate between variety comparisons to be made.
After six weeks of vernalization, plants were removed from the controlled environment rooms and grown in a glasshouse until the plants began to flower.

### Sampling Details

At each time point, trays of plants were sampled and the biological tissue pooled.
The first true leaf and the apex of each plant was taken and frozen initially on dry ice and then in liquid nitrogen.
While on dry ice, apicies were dissected to remove as much leaf and stem tissue as possible while leaving the apex intact.

### RNA Extraction Details

Tissue was ground and RNA extracted using the OMEGA bio-tek E.Z.N.A.® Plant RNA Kit in accordance with the manufacturer instructions.
An on-column DNase step was performed using a compatible product and method provided by OMEGA bio-tek.
RNA quality assurance and sequencing was conducted by The Earlham Institute, Norwich.

### Expression Quantification

The raw expression reads were analysed using the Tuxedo Suite of tools.
These consist of Bowtie, TopHat and Cufflinks.
Bowtie is an alignment algorithm which TopHat uses to align reads to a reference sequence.
TopHat is a splice-aware aligner, which is able to align sequencing reads that span a splice site to genomic reference sequence.
Finally, Cufflinks takes the positions of the aligned sequencing reads and performs quantification to give an expression level for each identified transcript.

The data available in this web app was generated using the available *Brassica napus* genome sequence as a reference sequence.
This sequence was generated from sequencing the *Brassica napus* cultivar Darmor-*bzh* (<a href="http://www.sciencemag.org/content/345/6199/950.full", target="_blank">Chalhoub et al. 2014</a>).
Gene models were determined directly from the sequencing data using a combination of Augustus and Cufflinks.

Cufflinks uses a statistical model to estimate the uncertainty of each expression measurement.
This model takes into account biological and technical variation through the use of repeat experiments, as well as read mapping uncertainty.
Read mapping uncertainty arises due to sequencing reads having multiple potential positions within the reference genome from which they could originate.
Repeat experiments, using different tissue pools sampled simultaneously and separate sequencing runs, were conducted for the majority of time points.
Cufflinks uses these repeat samples to estimate the error for samples without replication.
The complete sampling scheme used is available in <a href="https://doi.org/10.1111/tpj.14020", target="_blank">Jones et al. (2018)</a>.

### Mappings to published gene models

The *Brassica napus* gene models used to generate the dataset presented in ORDER used the collected RNA-Seq reads to inform gene model prediction.
To allow these novel gene models to be useful to the community, ORDER contains mappings from published gene models to the gene models used in ORDER.
The published gene models include:

- *Arabidopsis thaliana* - [TAIR10 gene models](ftp://ftp.arabidopsis.org/home/tair/Genes/TAIR10_genome_release)
- *Brassica oleracea* - [BOL gene models](ftp://ftp.ensemblgenomes.org/pub/plants/release-43/fasta/brassica_oleracea/cds) from release 43 of Ensembl Plants
- *Brassica rapa* - [BRAD version 3.0](http://brassicadb.org/brad/datasets/pub/BrassicaceaeGenome/Brassica_rapa/V3.0)
- *Brassica napus* - Pantranscriptome presented in [He et al. (2015)](https://doi.org/10.1016/j.dib.2015.06.016). To prevent confusion with duplicate gene model names, the gene models from the Pantranscriptome are prefixed with "Pan_".
